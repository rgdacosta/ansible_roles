<h1 id="vpn-system-role">VPN System Role</h1>

<p>A Role for managing setup and configuration of VPN tunnels.</p>

<p>Basic usage:</p>

<pre><code class="language-yaml">all:
  hosts:
    bastion1.example.com: {...}
    bastion2.example.com: {...}
    bastion3.example.com: {...}
  vars:
    vpn_connections:
      - hosts:
          bastion1.example.com:
          bastion2.example.com:
          bastion3.example.com:
</code></pre>

<p>The role will set up a vpn tunnel between each pair of hosts in the list of <code>vpn_connections</code>, using the default parameters, including generating keys as needed.  This role assumes that the names of the hosts under <code>hosts</code> are the same as the names of the hosts used in the Ansible inventory, and that you can use those names to configure the tunnels (i.e. they are real FQDNs that resolve correctly).</p>

<p>The exception to the above is when you define a <code>hostname</code> variable under any given host, containing an FQDN, in which case the role will assume this is a managed host and won’t attempt to make any changes to it (more details in <a href="#hosts">hosts</a>)</p>

<h2 id="requirements">Requirements</h2>

<p>The Ansible controller requires the python <code>ipaddress</code> package on EL7 systems,<br />
or other systems that use python 2.7.  On python 3.x systems, the VPN role<br />
uses the python3 built-in <code>ipaddress</code> module.</p>

<p>The role requires the <code>firewall</code> role and the <code>selinux</code> role from the<br />
<code>fedora.linux_system_roles</code> collection, if <code>vpn_manage_firewall</code><br />
and <code>vpn_manage_selinux</code> is set to true, respectively.<br />
(Please see also the variables in the <a href="#firewall-and-selinux"><code>Firewall and Selinux</code></a> section.)</p>

<p>If the <code>vpn</code> is a role from the <code>fedora.linux_system_roles</code><br />
collection or from the Fedora RPM package, the requirement is already<br />
satisfied.</p>

<p>Otherwise, please run the following command line to install the collection.</p>
<pre><code>ansible-galaxy collection install -r meta/collection-requirements.yml
</code></pre>

<h2 id="top-level-variables">Top-level variables</h2>

<p>These global variables should be applied to the configuration for every tunnel (unless the user overrides them in the configuration of a particular tunnel).</p>

<table>
  <thead>
    <tr>
      <th>Parameter</th>
      <th>Description</th>
      <th style="text-align: center">Type</th>
      <th style="text-align: center">Required</th>
      <th>Default</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>vpn_provider</td>
      <td>VPN provider used (e.g. libreswan, wireguard, etc.)</td>
      <td style="text-align: center">str</td>
      <td style="text-align: center">no</td>
      <td>libreswan</td>
    </tr>
    <tr>
      <td><a href="#vpn_auth_method">vpn_auth_method</a></td>
      <td>VPN authentication method used.</td>
      <td style="text-align: center">str</td>
      <td style="text-align: center">no</td>
      <td>psk</td>
    </tr>
    <tr>
      <td>vpn_regen_keys</td>
      <td>Whether pre-shared keys should be regenerated for sets of hosts with existing keys.</td>
      <td style="text-align: center">bool</td>
      <td style="text-align: center">no</td>
      <td>false</td>
    </tr>
    <tr>
      <td>vpn_opportunistic</td>
      <td>Whether an opportunistic mesh configuration should be used.</td>
      <td style="text-align: center">bool</td>
      <td style="text-align: center">no</td>
      <td>false</td>
    </tr>
    <tr>
      <td>vpn_default_policy</td>
      <td>The default policy group to add target machines to under a mesh configuration.</td>
      <td style="text-align: center">str</td>
      <td style="text-align: center">no</td>
      <td><code>private-or-clear</code></td>
    </tr>
    <tr>
      <td><a href="#vpn_ensure_openssl">vpn_ensure_openssl</a></td>
      <td>Ensure the <code>openssl</code> package is installed on the controller.</td>
      <td style="text-align: center">bool</td>
      <td style="text-align: center">no</td>
      <td>true</td>
    </tr>
    <tr>
      <td><a href="#vpn_connections">vpn_connections</a></td>
      <td>List of VPN connections to make.</td>
      <td style="text-align: center">list</td>
      <td style="text-align: center">yes</td>
      <td>-</td>
    </tr>
  </tbody>
</table>

<h3 id="vpn_auth_method">vpn_auth_method</h3>

<p>The value specified in this variable will determine the value of the <code>authby</code> field for the Libreswan tunnels opened.<br />
Acceptable values:</p>
<ul>
  <li><code>psk</code> for pre-shared key (PSK) authentication</li>
  <li><code>cert</code> for authentication using certificates</li>
</ul>

<h3 id="vpn_ensure_openssl">vpn_ensure_openssl</h3>

<p>The role uses <code>openssl</code> to generate PSKs.  It requires this to be installed on the controller node.<br />
The default value is <code>true</code>.  If you have pre-generated your PSKs, or you are not using PSKs, then<br />
set <code>vpn_ensure_openssl: false</code>. You can also define the PSKs using the <code>shared_key_content</code> variable in a host in any given tunnel.</p>

<h3 id="vpn_connections">vpn_connections</h3>

<p><code>vpn_connections</code> is a list of connections. Each connection is either:</p>

<ul>
  <li>
    <p>A list of hosts specified by <code>hosts</code>. In this host-to-host use case, the role creates tunnels between each pair of hosts. At least one tunnel must be defined in this list. If a single tunnel is required, you only need to specify the remote side.</p>
  </li>
  <li>
    <p>A mesh configuration consisting of one or more subnets and profiles. In this mesh use case, the role deploys an opportunistic mesh configuration using the <code>policy</code>/<code>cidr</code> pairs that you define in the <code>policies</code>.</p>
  </li>
</ul>

<h2 id="connection-specific-variables">Connection-specific variables</h2>

<p>In addition to the global variables, you may provide a number of other variables that will be applied to the configuration for each tunnel.<br />
<strong>NOTE</strong> All time fields (for example <code>ikelifetime</code> and others) accept the time as a number + unit e.g. <code>13h</code> for 13 hours, <code>10s</code> for 10 seconds.</p>

<table>
  <thead>
    <tr>
      <th>Parameter</th>
      <th>Description</th>
      <th style="text-align: center">Type</th>
      <th style="text-align: center">Required</th>
      <th>Default</th>
      <th style="text-align: center">Libreswan Equivalent</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href="#name">name</a></td>
      <td>A unique, arbitrary name used to prefix the connection name.</td>
      <td style="text-align: center">str</td>
      <td style="text-align: center">no</td>
      <td>See <a href="#name">name</a>.</td>
      <td style="text-align: center">conn <code>&lt;name&gt;</code></td>
    </tr>
    <tr>
      <td><a href="#hosts">hosts</a></td>
      <td>A VPN tunnel will be constructed between each pair of hosts in this dictionary.</td>
      <td style="text-align: center">dict</td>
      <td style="text-align: center">yes</td>
      <td>-</td>
      <td style="text-align: center">-</td>
    </tr>
    <tr>
      <td><a href="#auth_method">auth_method</a></td>
      <td>Authentication method to be used for this connection.</td>
      <td style="text-align: center">str</td>
      <td style="text-align: center">no</td>
      <td>vpn_auth_method</td>
      <td style="text-align: center">authby</td>
    </tr>
    <tr>
      <td><a href="#auto">auto</a></td>
      <td>What operation, if any, should be done automatically at startup.</td>
      <td style="text-align: center">str</td>
      <td style="text-align: center">no</td>
      <td>-</td>
      <td style="text-align: center">auto</td>
    </tr>
    <tr>
      <td><a href="#opportunistic">opportunistic</a></td>
      <td>Whether an opportunistic mesh configuration should be used.</td>
      <td style="text-align: center">bool</td>
      <td style="text-align: center">no</td>
      <td>vpn_opportunistic</td>
      <td style="text-align: center">-</td>
    </tr>
    <tr>
      <td><a href="#policies">policies</a></td>
      <td>List of policy settings to use for an opportunistic mesh configuration.</td>
      <td style="text-align: center">list</td>
      <td style="text-align: center">no</td>
      <td>-</td>
      <td style="text-align: center">-</td>
    </tr>
    <tr>
      <td>shared_key_content</td>
      <td>A pre-defined PSK. If not defined, the role will generate one using <code>openssl</code>. <strong>IMPORTANT:</strong> It is strongly suggested that you do not use this parameter, and instead let the role generate the values.  If you must use this, do not set a string in your inventory, but instead read this from a Vault. Also, the PSK will be visible while running in verbose or debug mode.</td>
      <td style="text-align: center">str</td>
      <td style="text-align: center">no</td>
      <td>-</td>
      <td style="text-align: center">PSK from ipsec.secrets file</td>
    </tr>
    <tr>
      <td>ike</td>
      <td>IKE encryption/authentication algorithm to be used for the connection (phase 1 aka ISAKMP SA). <strong>NOTE</strong> Do not set this unless you must, or really know what you are doing</td>
      <td style="text-align: center">str</td>
      <td style="text-align: center">no</td>
      <td>-</td>
      <td style="text-align: center">ike</td>
    </tr>
    <tr>
      <td>esp</td>
      <td>Specifies the algorithms that will be offered/accepted for a Child SA negotiation. <strong>NOTE</strong> Do not set this unless you must, or really know what you are doing</td>
      <td style="text-align: center">str</td>
      <td style="text-align: center">no</td>
      <td>-</td>
      <td style="text-align: center">esp</td>
    </tr>
    <tr>
      <td>type</td>
      <td>The type of the connection.  See the libreswan docs for the possible values</td>
      <td style="text-align: center">str</td>
      <td style="text-align: center">no</td>
      <td>tunnel</td>
      <td style="text-align: center">type</td>
    </tr>
    <tr>
      <td>ikelifetime</td>
      <td>How long the keying channel of a connection (buzzphrase: “IKE SA” or “Parent SA”) should last before being renegotiated.</td>
      <td style="text-align: center">str</td>
      <td style="text-align: center">no</td>
      <td>-</td>
      <td style="text-align: center">ikelifetime</td>
    </tr>
    <tr>
      <td>salifetime</td>
      <td>How long a particular instance of a connection (a set of encryption/authentication keys for user packets) should last, from successful negotiation to expiry.</td>
      <td style="text-align: center">str</td>
      <td style="text-align: center">no</td>
      <td>-</td>
      <td style="text-align: center">salifetime</td>
    </tr>
    <tr>
      <td>retransmit_timeout</td>
      <td>How long a single packet, including retransmits of that packet, may take before the IKE attempt is aborted.</td>
      <td style="text-align: center">str</td>
      <td style="text-align: center">no</td>
      <td>-</td>
      <td style="text-align: center">retransmit-timeout</td>
    </tr>
    <tr>
      <td>dpddelay</td>
      <td>Set the delay time between Dead Peer Detection (IKEv1 RFC 3706) or IKEv2 Liveness keepalives that are sent for this connection.  If this is set, dpdtimeout also needs to be set</td>
      <td style="text-align: center">str</td>
      <td style="text-align: center">no</td>
      <td>-</td>
      <td style="text-align: center">dpddelay</td>
    </tr>
    <tr>
      <td>dpdtimeout</td>
      <td>Set the length of time that we will idle without hearing back from our peer. After this period has elapsed with no response and no traffic, we will declare the peer dead, and remove the SA. Set value bigger than dpddelay to enable. If dpdtimeout is set, dpddelay also needs to be set.</td>
      <td style="text-align: center">str</td>
      <td style="text-align: center">no</td>
      <td>-</td>
      <td style="text-align: center">dpdtimeout</td>
    </tr>
    <tr>
      <td>dpdaction</td>
      <td>When a DPD enabled peer is declared dead, what action should be taken.  See libreswan docs for values.</td>
      <td style="text-align: center">str</td>
      <td style="text-align: center">no</td>
      <td>-</td>
      <td style="text-align: center">dpdaction</td>
    </tr>
    <tr>
      <td><a href="#leftupdown">leftupdown</a></td>
      <td>The “updown” script to run to adjust routing and/or firewalling when the status of the connection changes (default <code>ipsec _updown</code>).  See below.</td>
      <td style="text-align: center">str</td>
      <td style="text-align: center">no</td>
      <td>-</td>
      <td style="text-align: center">leftupdown</td>
    </tr>
  </tbody>
</table>

<p>For the default values, and possible values, of <code>ike</code>, <code>esp</code>, <code>type</code>, et. al., please consult the <a href="https://libreswan.org/man/ipsec.conf.5.html">libreswan documentation</a>.  You will usually not need to set these.</p>

<h3 id="name">name</h3>

<p>By default, the role generates a descriptive name for each tunnel it creates from the perspective of each system. For example, when creating a tunnel between <code>bastion1</code> and <code>bastion2</code>, the descriptive name of this connection on <code>bastion1</code> is <code>bastion1-to-bastion2</code> but on <code>bastion2</code> the connection is named <code>bastion2-to-bastion1</code>. You may add a prefix to these auto-generated names by specifying a value in the <code>name</code> field.</p>

<h3 id="auth_method">auth_method</h3>

<p>Optionally, you can define an authentication method to use at the connection level. If <code>auth_method</code> is not defined, the role uses the global variable <code>vpn_auth_method</code>. The value of <code>auth_method</code>, or <code>vpn_auth_method</code>, determines the value of the <code>authby</code> field for the Libreswan tunnel opened for this connection. Acceptable values:</p>
<ul>
  <li><code>psk</code> for pre-shared key (PSK) authentication</li>
  <li><code>cert</code> for authentication using certificates</li>
</ul>

<h3 id="auto">auto</h3>

<p>What operation, if any, should be done automatically at IPsec startup. Currently accepted values are <strong>add</strong>, <strong>ondemand</strong>, <strong>start</strong>, and <strong>ignore</strong>. The default value is null, which means no automatic startup operation.</p>

<h3 id="opportunistic">opportunistic</h3>

<p>By default, the VPN System Role creates a host-to-host tunnel between each pair of nodes specified within a <code>vpn_connection</code>. You can instead configure an opportunistic mesh VPN by setting <code>opportunistic</code> to <code>true</code>, which will include all hosts in the Ansible inventory in the opportunistic mesh configuration.</p>

<p><strong>Note:</strong> When configuring an opportunistic mesh VPN using a control node that shares the same CIDR as one or more of mesh CIDRs used for encryption, add a clear policy entry for the control node CIDR in order to prevent an SSH connection loss during the play. See <a href="#opportunistic-mesh-vpn-configuration">example</a>.</p>

<h3 id="leftupdown">leftupdown</h3>

<p>It is best to keep it simple - no arguments with spaces, shell metacharacters, or other characters which require quoting or escaping - it will be<br />
difficult to pass them through the various layers of yaml, ansible, jinja, and shell.  Example:</p>
<pre><code class="language-yaml">  leftupdown: ipsec_updown --route yes
</code></pre>
<p>will result in the config file</p>
<pre><code>leftupdown="ipsec_updown --route yes"
</code></pre>
<p>If you need to pass an argument which requires quoting, use single quotes:</p>
<pre><code>  leftupdown: ipsec_updown --route 'a quoted route value'
</code></pre>
<p>will result in the config file</p>
<pre><code>leftupdown="ipsec_updown --route 'a quoted route value'"
</code></pre>
<p>If you need a custom script, the role does not current have the ability to copy or create a script on the managed host.  You’ll have to figure<br />
out some way to place the script on the host. Then you can point to the script using the full path, like <code>/usr/local/bin/myscript</code>.</p>

<p>By default, Libreswan runs <code>ipsec_updown --route yes</code>. You can disable that by using <code>leftupdown: null</code>.</p>

<h3 id="policies">policies</h3>

<p>In this dictionary, you can set policy rules related to opportunistic encryption. If no policy rules are set, the default policy rule is <code>private-or-clear</code>. To override this default policy rule, see <a href="#cidr">cidr</a>. Note that the default policy does not add a <code>0.0.0.0/0</code> entry into a policy file. Instead, individual classless inter-domain routing (CIDR) values are added to policy files based on the CIDRs of the managed nodes. The default policy rule will be applied to CIDRs of all the hosts over which this role is run, unless you specify in this section a different policy rule for the CIDR of a particular managed node or group of managed nodes. If users wish to add a <code>0.0.0.0/0</code> entry to a particular policy file, they may add an item to this list where the policy value is the desired policy to be applied, and the CIDR value is <code>0.0.0.0/0</code>.</p>

<table>
  <thead>
    <tr>
      <th>Parameter</th>
      <th>Description</th>
      <th style="text-align: center">Type</th>
      <th style="text-align: center">Required</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href="#policy">policy</a></td>
      <td>A valid policy connection group.</td>
      <td style="text-align: center">str</td>
      <td style="text-align: center">no</td>
    </tr>
    <tr>
      <td><a href="#cidr">cidr</a></td>
      <td>A valid CIDR to which this policy rule is applied.</td>
      <td style="text-align: center">str</td>
      <td style="text-align: center">no</td>
    </tr>
  </tbody>
</table>

<h4 id="policy">policy</h4>

<p>Valid values are <code>private</code>, <code>private-or-clear</code>, and <code>clear</code>.</p>

<h4 id="cidr">cidr</h4>

<p>In addition to any valid CIDR value, you may specify <code>default</code> in this field to apply the corresponding policy to all hosts that do not fit into one of the other specified policy groups, thereby overriding the default private-or-clear policy rule.</p>

<h3 id="hosts">hosts</h3>
<p>Each key in this dictionary is the unique name of a host. If a host is listed in <code>hosts</code> and not in the inventory file, the host will not be managed by the inventory. In such case, the <code>hostname</code> parameter is required because it is necessary for setting up the local ends of such a tunnel.</p>

<p>If the host key in the hosts list of your inventory is not the fully qualified domain name (FQDN) you want to use, you must use the <code>hostname</code> field under each host in this <code>vpn_connections</code> hosts dictionary to specify the actual FQDN or IP address you want the VPN role to use for setting up the tunnel. If you do not specify <code>hostname</code>, then the role will use <code>ansible_host</code> if defined, or the host key in your hosts list if neither <code>ansible_host</code> nor <code>hostname</code> is defined.</p>

<p>For each host key in this dictionary, the following host-specific parameters can be specified.</p>

<table>
  <thead>
    <tr>
      <th>Parameter</th>
      <th>Description</th>
      <th style="text-align: center">Type</th>
      <th style="text-align: center">Required</th>
      <th>Default</th>
      <th style="text-align: center">Libreswan Equivalent</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href="#hostname">hostname</a></td>
      <td>Host name or IP address to use for setting up a VPN connection.</td>
      <td style="text-align: center">str</td>
      <td style="text-align: center">no</td>
      <td>-</td>
      <td style="text-align: center">left/right</td>
    </tr>
    <tr>
      <td><a href="#cert_name">cert_name</a></td>
      <td>Certificate nickname of this host’s certificate in the NSS database. (Only used when <code>auth_method</code> is <code>cert</code>)</td>
      <td style="text-align: center">str</td>
      <td style="text-align: center">no</td>
      <td>-</td>
      <td style="text-align: center">leftcert/rightcert</td>
    </tr>
    <tr>
      <td>subnets</td>
      <td>A list of the subnets that should be available via the VPN connection.</td>
      <td style="text-align: center">list</td>
      <td style="text-align: center">no</td>
      <td>-</td>
      <td style="text-align: center">leftsubnets/rightsubnets</td>
    </tr>
    <tr>
      <td>leftid</td>
      <td>How the left participant (local) should be identified for authentication.</td>
      <td style="text-align: center">str</td>
      <td style="text-align: center">no</td>
      <td>the local host FQDN (not the controller)</td>
      <td style="text-align: center">leftid</td>
    </tr>
    <tr>
      <td>rightid</td>
      <td>How the right participant (remote) should be identified for authentication.</td>
      <td style="text-align: center">str</td>
      <td style="text-align: center">no</td>
      <td>the remote host FQDN</td>
      <td style="text-align: center">rightid</td>
    </tr>
  </tbody>
</table>

<h4 id="hostname">hostname</h4>

<p>Can hold a host name or IP address. Specified only when overriding host names used by Ansible for SSH. Note that if a host name is specified, it must be fully qualified to ensure that DNS resolution works correctly on host machines. This parameter is required when the host is not part of the inventory list of hosts.</p>

<h4 id="cert_name">cert_name</h4>
<p>It is assumed that the <code>cert_name</code> provided by the user exists in the IPSec NSS cert database. Users may use the certificate system role to issue these certificates.</p>

<h2 id="verifying-a-successful-startup">Verifying a successful startup</h2>

<h3 id="libreswan">Libreswan</h3>

<p>To confirm that a connection is successfully loaded:</p>
<pre><code>ipsec status | grep &lt;connectionname&gt;
</code></pre>

<p>To confirm that a connection is successfully started:</p>
<pre><code>ipsec trafficstatus | grep &lt;connectionname&gt;
</code></pre>

<p>To verify that a certificate has been imported (requires that the connection has loaded successfully). Note that if the same certificate is used for multiple connections, it may show up in the output for this command, even though there was an error on the connection being checked:</p>
<pre><code>ipsec whack --listcerts
</code></pre>

<p>If a connection did not successfully load, it is recommended to run the following command to manually try to add the connection. This will give more specific information indicating why the connection failed to establish:</p>

<pre><code>ipsec auto --add &lt;connectionname&gt;
</code></pre>

<p>Any errors that may have occurred during the process of loading and starting the connection are in the logs, which can be found in <code>/var/log/pluto.log</code> in RHEL 8, or by issuing the command <code>journalctl -u ipsec</code> in RHEL 7. Since these logs can be verbose and contain old entries, it is generally recommended to try to manually add the connection to obtain log messages from the standard output instead.</p>

<h2 id="firewall-and-selinux">Firewall and Selinux</h2>

<p>The firewall must be configured to allow traffic on 500/UDP, 4500/UDP, and 4500/TCP ports for the IKE, ESP, and AH protocols.</p>

<table>
  <thead>
    <tr>
      <th>Parameter</th>
      <th>Description</th>
      <th style="text-align: center">Type</th>
      <th style="text-align: center">Required</th>
      <th>Default</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>vpn_manage_firewall</td>
      <td>If true, enable the IPsec ports, 500/UDP, 4500/UDP, and 4500/TCP for the IKE, ESP, and AH protocols using the firewall role. If false, the <code>vpn role</code> does not manage the firewall.</td>
      <td style="text-align: center">bool</td>
      <td style="text-align: center">no</td>
      <td>false</td>
    </tr>
    <tr>
      <td>vpn_manage_selinux</td>
      <td>If true, manage the IPsec ports, 500/UDP, 4500/UDP, and 4500/TCP using the selinux role. If false, the <code>vpn role</code> does not manage the selinux.</td>
      <td style="text-align: center">bool</td>
      <td style="text-align: center">no</td>
      <td>false</td>
    </tr>
  </tbody>
</table>

<p>NOTE: The firewall configuration is prerequisite for managing selinux. If the<br />
firewall is not installed, managing selinux policy is skipped.</p>

<p>NOTE: <code>vpn_manage_firewall</code> and <code>vpn_manage_selinux</code> are limited to<br />
<em>adding</em> ports and policy, respectively.<br />
It cannot be used for <em>removing</em> them.<br />
If you want to remove ports and/or, you will need to use the firewall system<br />
role and/or the selinux role directly.</p>

<h2 id="use-cases">Use Cases</h2>

<ul>
  <li>Host-to-Host (openstack): Specific nodes connecting to each other. Use IPsec for IP failover between these nodes (so all other nodes don’t need to be aware of anything happening). Keys are FreeIPA certificates, and pre-shared keys</li>
  <li>Host-to-Host (data centers): Two systems in different data centers communicate encrypted with each other using FreeIPA certificates, and pre-shared keys</li>
  <li>Host-to-Host (one host): One system communicating with an existing system (e.g., cisco) in an other organization that uses pre-shared keys</li>
  <li>Network-to-Network (two routers): One organization router connecting to a second one bringing together two distinct networks. Keys are FreeIPA certificates, and pre-shared keys.</li>
  <li>VPN Remote Access Server / Roadwarrior: One organization router accepting connections from multiple clients. Clients connect to a single router using FreeIPA certificates.</li>
  <li>MESH: node independent configurations. When adding/removing a node, you don’t need to reconfigure all other nodes. They all attempt to setup individual host-to-host connections. A PKI is used to authenticate nodes (FreeIPA, potentially in the future DNSSEC)</li>
</ul>

<p>Note that for a couple of these use cases, you cannot use host-scoped settings (e.g. global settings specified in <code>all.hosts</code>).</p>

<h2 id="examples">Examples</h2>

<h3 id="host-to-host-multiple-vpn-tunnels-with-one-externally-managed-host">Host-to-host (multiple VPN tunnels with one externally managed host)</h3>

<p>This playbook sets up the tunnel <code>bastion_east-to-bastion_west</code> using pre-shared key authentication with keys auto-generated by the system role. Additionally, the local ends of two more tunnels are set up: <code>bastion_east-to-bastion_north</code> and <code>bastion_west-to-bastion_north</code>. In this case, one of the hosts, <code>bastion_north</code>, is external to the inventory e.g. in a remote datacenter, and only the local ends of the tunnels can be set up. The <code>hostname</code> field contains all the information necessary to ensure that the local ends of the tunnel are set up correctly.  This also shows the<br />
optional parameters you can specify for the tunnel.</p>

<pre><code class="language-yaml">all:
  hosts:
    bastion_east:
      ansible_host: bastion1.example.com
    bastion_west:
      ansible_host: bastion2.example.com
  vars:
    vpn_connections:
      - ike: aes256-sha2;dh19
        esp: aes-sha2_512+sha2_256
        ikelifetime: 11h
        salifetime: 9h
        type: transport
        hosts:
          bastion_east:
          bastion_west:
          bastion_north: # not in the hosts list
            hostname: 192.168.122.103
</code></pre>

<h3 id="host-to-host-multiple-vpn-tunnels-with-multiple-nics">Host-to-host (multiple VPN tunnels with multiple NICS)</h3>

<p>In this case, the hosts have multiple vpn connections associated with multiple NICs e.g. some OpenStack and OpenShift use cases.</p>

<pre><code class="language-yaml">all:
  hosts:
    bastion_east: {...}
    bastion_west: {...}
    bastion_north: {...}
  vars:
    vpn_connections:
      - name: control_plane_vpn
        hosts:
          bastion_east:
            hostname: 192.168.122.101 # IP for control plane
          bastion_west:
            hostname: 192.168.122.102
          bastion_north:
            hostname: 192.168.122.103
      - name: data_plane_vpn
        hosts:
          bastion_east:
            hostname: 10.0.0.1 # IP for data plane
          bastion_west:
            hostname: 10.0.0.2
          bastion_north:
            hostname: 10.0.0.3
</code></pre>

<h3 id="host-to-host-multiple-vpn-tunnels-using-certificates">Host-to-host (multiple VPN tunnels using certificates)</h3>

<p>This playbook sets up host-to-host tunnels between each pair of hosts in the list of <code>hosts</code> using certificates for authentication.</p>

<pre><code class="language-yaml">  hosts:
    bastion1.example.com: {...}
    bastion2.example.com: {...}
    bastion3.example.com: {...}
  vars:
    vpn_connections:
      - name: vpn-tunnel-x
        auth_method: cert
        auto: start
        hosts:
          bastion1.example.com:
            cert_name: bastion1cert
          bastion2.example.com:
            cert_name: bastion2cert
          bastion3.example.com:
            cert_name: bastion3cert
</code></pre>

<h3 id="managed-host-to-unmanaged-host-eg-remote-is-appliance">Managed-host-to-unmanaged-host (e.g. remote is appliance)</h3>

<p>This playbook sets up a host-to-host tunnel between the current host in the inventory, and a remote host not managed by Ansible (like an appliance) which requires proper identification. In this example <code>this_host</code> should be manually set with the same name as <code>inventory_hostname</code>.  The shared key is the key shared between the hosts.</p>

<pre><code class="language-yaml">  vars:
    vpn_connections:
      - auth_method: psk
        auto: start
        shared_key_content: !vault |
          $ANSIBLE_VAULT;1.2;AES256;dev
          ....
        hosts:
          this_host:
            leftid: idoftheclient
          nfsserver:
            hostname: nfsserver.example.com
            rightid: idoftheserver
</code></pre>

<h3 id="opportunistic-mesh-vpn-configuration">Opportunistic Mesh VPN configuration</h3>

<p>This playbook sets up an opportunistic mesh VPN configuration on each host in the list of <code>hosts</code>, using certificates for authentication. In this example, the controller machine shares the same CIDR as both of the target machines (<code>192.168.110.0/24</code>) and has IP address <code>192.168.110.7</code>. Therefore the controller machine will fall under a <code>private</code> policy which will automatically be created for the CIDR <code>192.168.110.0/24</code>. To prevent an SSH connection loss during the play, a <code>clear</code> policy for the controller machine has been added to the list of  <code>policies</code>. Note that there is also an item in the <code>policies</code> list where the <code>cidr</code> is equal to <code>default</code>. This is because this playbook is overriding the default policy rule to make it <code>private</code> instead of <code>private-or-clear</code>.</p>

<pre><code class="language-yaml">  hosts:
    bastion1.example.com:
      cert_name: bastion1cert
    bastion2.example.com:
      cert_name: bastion2cert
    bastion3.example.com:
      cert_name: bastion3cert
  vars:
    vpn_connections:
      - opportunistic: true
        auth_method: cert
        policies:
          - policy: private
            cidr: default
          - policy: private-or-clear
            cidr: 192.168.122.0/24
          - policy: private
            cidr: 192.168.110.0/24
          - policy: clear
            cidr: 192.168.110.7/32         
</code></pre>

<h2 id="to-be-added-in-a-future-release">To be added in a future release</h2>

<p>The following global variables will be added. Additionally, <code>pubkey</code> will be added as a valid option under <code>vpn_auth_method</code> to perform public key authentication without certificates (enforces SHA-2).</p>

<table>
  <thead>
    <tr>
      <th>Parameter</th>
      <th>Description</th>
      <th style="text-align: center">Type</th>
      <th style="text-align: center">Required</th>
      <th>Default</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>vpn_enc_alg</td>
      <td>VPN encryption algorithm to use. See <a href="#algorithms">Algorithms section</a> for acceptable values.</td>
      <td style="text-align: center">str</td>
      <td style="text-align: center">no</td>
      <td>-</td>
    </tr>
    <tr>
      <td>vpn_auth_alg</td>
      <td>VPN authentication algorithm to use.</td>
      <td style="text-align: center">str</td>
      <td style="text-align: center">no</td>
      <td>SHA-2</td>
    </tr>
    <tr>
      <td>vpn_wait</td>
      <td>If tasks should wait for the VPN tunnel to be started up.</td>
      <td style="text-align: center">bool</td>
      <td style="text-align: center">no</td>
      <td>false</td>
    </tr>
    <tr>
      <td>vpn_public_key_src</td>
      <td>Path to file on the controller host containing public key used by default.</td>
      <td style="text-align: center">str</td>
      <td style="text-align: center">no</td>
      <td>-</td>
    </tr>
    <tr>
      <td>vpn_public_key_content</td>
      <td>Contains the public key used by default for public key authentication without certificates.</td>
      <td style="text-align: center">str</td>
      <td style="text-align: center">no</td>
      <td>-</td>
    </tr>
  </tbody>
</table>

<p>The following variables will be added under the <a href="#hosts"><code>hosts</code></a> dictionary:</p>

<table>
  <thead>
    <tr>
      <th>Parameter</th>
      <th>Description</th>
      <th style="text-align: center">Type</th>
      <th style="text-align: center">Required</th>
      <th>Default</th>
      <th style="text-align: center">Libreswan Equivalent</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href="#public_key">public_key_src</a></td>
      <td>Path to file on the controller host containing public key used by this host.</td>
      <td style="text-align: center">str</td>
      <td style="text-align: center">no</td>
      <td>-</td>
      <td style="text-align: center">leftrsasigkey/rightrsasigkey</td>
    </tr>
    <tr>
      <td><a href="#public_key">public_key_content</a></td>
      <td>Contains the public key used by this host for public key authentication without certificates.</td>
      <td style="text-align: center">str</td>
      <td style="text-align: center">no</td>
      <td>-</td>
      <td style="text-align: center">leftrsasigkey/rightrsasigkey</td>
    </tr>
  </tbody>
</table>

<h3 id="shared_key">shared_key</h3>

<p><code>shared_key_src</code> indicates the path to a file on the controller host containing a PSK to be copied to the <code>ipsec.secrets</code> file on the managed node.</p>

<p><strong>Notes: It is recommended to not specify a pre-shared key, since the role will automatically generate a secure pre-shared key if none is provided by the user. If the user does wish to provide their own pre-shared key, the recommendation is to vault encrypt the value. See https://docs.ansible.com/ansible/latest/user_guide/vault.html. Also, since it is still unclear how the role will allow users to specific pre-shared keys for each pair of hosts in a tunnel, it is reiterated that users should rely on the role’s ability to generate secure pre-shared keys automatically.</strong></p>

<h3 id="public_key">public_key</h3>

<p><code>public_key_src</code> specifies a path to a file on the controller host containing the public key used by this host for public key authentication without certificates. Otherwise, the user can directly specify the public key for this host by populating <code>public_key_content</code>. <code>public_key_content</code> can also accept a CKAID or nickname for a public key in the NSS database.</p>

<p>Note that <code>public_key_src</code> and <code>public_key_content</code> may also be specified as host-scoped Ansible variables. The variable names in this case will be <code>vpn_public_key_src</code> and <code>vpn_public_key_content</code>.</p>

<p>If neither <code>public_key_src</code> nor <code>public_key_content</code> is populated, the role will generate key pairs for each host.</p>

<h3 id="algorithms">Algorithms</h3>

<h4 id="libreswan-1">Libreswan</h4>
<p>Minimum acceptable algorithms are AES, MODP2048 and SHA2.</p>

<h2 id="license">License</h2>

<p>MIT.</p>
