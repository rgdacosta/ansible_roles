<h1 id="rhc">rhc</h1>
<p><img src="https://github.com/linux-system-roles/template/workflows/tox/badge.svg" alt="rhc" /></p>

<p>An ansible role which connects RHEL systems to Red Hat.</p>

<h2 id="requirements">Requirements</h2>

<p>The role requires subscription-manager, which is available from the standard<br />
RHEL repositories, and usually installed by default on RHEL. On other<br />
distributions it will be installed if not already.</p>

<p>The role requires also insights-client, which is available from the standard<br />
RHEL repositories, in case the Insights support is enabled (and it is by<br />
default).</p>

<p>In addition, the role requires rhc, which is available from the standard RHEL<br />
repositories, in case the Insights remediation is enabled (and it is by<br />
default).</p>

<p>The role requires modules from <code>community.general</code>.  If you are using<br />
<code>ansible-core</code>, you must install the <code>community.general</code> collection.  Use the<br />
file <code>meta/collection-requirements.yml</code> to install it:</p>
<pre><code>ansible-galaxy collection install -vv -r meta/collection-requirements.yml
</code></pre>
<p>If you are using Ansible Engine 2.9, or are using an Ansible bundle which<br />
includes these collections/modules, you should have to do nothing.</p>

<h2 id="role-variables">Role Variables</h2>

<pre><code class="language-yaml">    rhc_state: present
</code></pre>

<p>Whether the system is connected to Red Hat; valid values are <code>present</code><br />
(the default, to ensure connection), <code>absent</code>, and <code>reconnect</code>.</p>

<p>When using <code>reconnect</code>, the system will be first disconnected in case<br />
it was already connected; because of this, the role will always report a<br />
“changed” status.</p>

<pre><code class="language-yaml">    rhc_organization: "your-organization"
</code></pre>

<p>The organization of the user. This <em>must</em> be specified when connecting if<br />
either:</p>
<ul>
  <li>the user belongs to more than one organization</li>
  <li>using activation keys (see <code>rhc_auth</code> below)</li>
</ul>

<pre><code class="language-yaml">    rhc_auth: {}
</code></pre>

<p>The authentication method used to connect a system. This must be specified<br />
in case a system may need to connect (e.g. in case it was not before).<br />
There are few possible authentication methods; only one can be specified at<br />
a time.</p>

<p><strong>NB</strong>: the variables used for authentication are considered secrets, and thus<br />
they ought to be secured. We recommend the usage of Ansible Vault as source<br />
for them. The references below only describe which keys exists and what they<br />
are for.</p>

<p>For authenticating using username &amp; password, specify the <code>login</code> dictionary<br />
using the following mandatory keys:</p>
<pre><code class="language-yaml">rhc_auth:
  login:
    username: "your-username"
    password: "your-password"
</code></pre>
<p>using <code>rhc_organization</code> if needed.</p>

<p>For authenticating using activation keys, specify the <code>activation_keys</code><br />
dictionary using the following mandatory keys, together with <code>rhc_organization</code>:</p>
<pre><code class="language-yaml">rhc_auth:
  activation_keys:
    keys: ["key-1", ...]
rhc_organization: "your-organization"
</code></pre>

<pre><code class="language-yaml">    rhc_server: {}
</code></pre>

<p>The details of the registration server to connect to; it can contain the<br />
following optional keys:</p>
<pre><code class="language-yaml">rhc_server:
  hostname: "hostname"
  port: 443
  prefix: "server-prefix"
  insecure: false
</code></pre>
<ul>
  <li><code>hostname</code> is the hostname of the server</li>
  <li><code>port</code> is the port to which connect to on the server</li>
  <li><code>prefix</code> is the prefix (starting with <code>/</code>) for the API calls to the server</li>
  <li><code>insecure</code> specifies whether to disable the validation of the SSL certificate<br />
of the server</li>
</ul>

<pre><code class="language-yaml">    rhc_baseurl: ""
</code></pre>

<p>The base URL for receiving content from the subscription server.</p>

<pre><code class="language-yaml">    rhc_repositories: []
</code></pre>

<p>A list of repositories to enable or disable in the system. Each item is a<br />
dictionary containing two keys:</p>
<ul>
  <li><code>name</code> is the name of a repository; this keys is mandatory</li>
  <li><code>state</code> is the state of that repository in the system, and it can be <code>enabled</code><br />
or <code>disabled</code>; this key is optional, and <code>enabled</code> if not specified<br />
```yaml<br />
rhc_repositories:
    <ul>
      <li>{name: “repository-1”, state: enabled}</li>
      <li>{name: “repository-2”, state: disabled}<br />
```</li>
    </ul>
  </li>
</ul>

<pre><code class="language-yaml">    rhc_release: "release"
</code></pre>

<p>A release to set for the system. Typically used for locking a RHEL system to<br />
a certain minor version of RHEL.</p>

<p>Use <code>{"state":"absent"}</code> (and not <code>""</code>) to actually unset the release set for<br />
the system.</p>

<pre><code class="language-yaml">    rhc_insights:
      state: present
</code></pre>

<p>Whether the system is connected to Insights; valid values are <code>present</code><br />
(the default, to ensure connection), and <code>absent</code>.</p>

<pre><code class="language-yaml">    rhc_insights:
      autoupdate: true
</code></pre>

<p>Whether the system automatically updates the dynamic configuration. It is<br />
enabled by default.</p>

<pre><code class="language-yaml">    rhc_insights:
      remediation: present
</code></pre>

<p>Whether the system is configured to run the Insights remediation; valid values<br />
are <code>present</code> (the default, to ensure remediation), and <code>absent</code>.</p>

<pre><code class="language-yaml">    rhc_insights:
      tags: {}
</code></pre>

<p>A dictionary of tags that is added to the system record in Host Based Inventory<br />
(HBI); typically used for the grouping and tagging of systems, and to search<br />
for systems in the inventory.</p>

<p>Possible values of this variable:</p>
<ul>
  <li><code>null</code> or an empty value (e.g.: <code>{}</code>): the tags file content is not changed</li>
  <li><code>{state: absent}</code>: all the tags are removed (by removing the tags file)</li>
  <li>any other value: the file is created with the specified tags</li>
</ul>

<p>Since the tags are arbitrary values for the tagging of systems, there is no<br />
fixed format. In the specified dictionary, the keys are strings, and the type<br />
of the values can be any data type (strings, numbers, lists, dictionaries,<br />
etc).</p>

<p>Example of the tags configured in the <code>insights-client</code><br />
<a href="https://access.redhat.com/documentation/en-us/red_hat_insights/2023/html/client_configuration_guide_for_red_hat_insights/con-insights-client-tagging-overview_insights-cg-adding-tags">documentation</a>:</p>
<pre><code class="language-yaml">rhc_insights:
  tags:
    group: _group-name-value_
    location: _location-name-value_
    description:
      - RHEL8
      - SAP
    key 4: value
</code></pre>

<pre><code class="language-yaml">    rhc_proxy: {}
</code></pre>

<p>The details of the proxy server to use for connecting:</p>
<pre><code class="language-yaml">rhc_proxy:
  hostname: "proxy-hostname"
  port: 4321
  username: "proxy-hostname"
  password: "proxy-password"
</code></pre>
<ul>
  <li><code>hostname</code> is the hostname of the proxy server</li>
  <li><code>port</code> is the port to which connect to on the proxy server</li>
  <li><code>username</code> is the username to use for authenticating on the proxy server;<br />
it can be not specified if the proxy server does not require authentication</li>
  <li><code>password</code> is the password to use for authenticating on the proxy server;<br />
it can be not specified if the proxy server does not require authentication</li>
</ul>

<p>Use <code>{"state":"absent"}</code> to reset all the proxy configurations to empty<br />
(effectively disabling the proxy server).</p>

<p><strong>NB</strong>: the variables used for the authentication on the proxy server are<br />
considered secrets, and thus they ought to be secured. We recommend the usage<br />
of Ansible Vault as source for them.</p>

<pre><code class="language-yaml">    rhc_environments: []
</code></pre>

<p>The list of environments to which register to when connecting the system.</p>

<p><em>NB</em>:</p>
<ul>
  <li>this only works when the system is being connected from an unconnected state<br />
– it cannot change the environments of already connected systems</li>
  <li>this requires the environments to be enabled on the registration server;<br />
in Red Hat Satellite or Katello, this feature is called “Content Views”</li>
</ul>

<h2 id="dependencies">Dependencies</h2>

<p>None.</p>

<h2 id="example-playbooks">Example Playbooks</h2>

<p>Ensure the connection to Red Hat including Insights, authenticating using<br />
username &amp; password:</p>

<pre><code class="language-yaml">- hosts: all
  vars:
    rhc_auth:
      login:
        username: "your-username"
        password: !vault |
          $ANSIBLE_VAULT;1.2;AES256;dev
          ....
  roles:
    - rhel-system-roles.rhc
</code></pre>

<p>Ensure that certain RHEL 9 repositories are enabled, and another one is not:</p>

<pre><code class="language-yaml">- hosts: all
  vars:
    rhc_repositories:
      - {name: "rhel-9-for-x86_64-baseos-rpms", state: enabled}
      - {name: "rhel-9-for-x86_64-appstream-rpms", state: enabled}
      - {name: "codeready-builder-for-rhel-9-x86_64-rpms", state: disabled}
  roles:
    - rhel-system-roles.rhc
</code></pre>

<p>Ensure that a RHEL 8 system is locked on RHEL 8.6:</p>

<pre><code class="language-yaml">- hosts: all
  vars:
    rhc_release: 8.6
  roles:
    - rhel-system-roles.rhc
</code></pre>

<p>Ensure that a system is connected to Insights, without optional features such<br />
as automatic updates and remediation:</p>

<pre><code class="language-yaml">- hosts: all
  vars:
    rhc_insights:
      autoupdate: false
      remediation: absent
  roles:
    - rhel-system-roles.rhc
</code></pre>

<h2 id="license">License</h2>

<p>MIT</p>
