# ansible_roles



## Getting started

[1] Clone the repository:

```
git clone https://gitlab.com/rgdacosta/ansible_roles.git
```

[2] Update the variables below group_vars/ and host_vars/

[3] Run the playbook (with or without an execution environment:

```
ansible-playbook postprovision.yml
```

or 

```
ansible-navigator run -m stdout postprovision.yml
```

Problems? rdacosta@redhat.com
